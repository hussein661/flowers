// back/src/db.js
import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

// back/src/db.js
const nowForSQLite = () => new Date().toISOString().replace('T',' ').replace('Z','')
const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');

  const createUserIfNotExists = async props => {
    const { auth0_sub, nickname } = props;
    const answer = await db.get(
      SQL`SELECT user_id FROM users WHERE auth0_sub = ${auth0_sub}`
    );
    if (!answer) {
      await createUser(props)
      return {...props, firstTime:true } // if the user didn't exist, make that clear somehow
    }
    return props;
  };
  const createUser = async props => {
    const { auth0_sub, nickname } = props;
    const result = await db.run(SQL`INSERT INTO users (auth0_sub, nickname) VALUES (${auth0_sub},${nickname});`);
    return result.stmt.lastID;
  }


  const addFlower = async (props) => {

    const { type, alive, color } = props
    try{
      const result = await db.run(SQL`INSERT INTO Flowers (type,alive,color,date,seller_id) VALUES (${type}, ${alive},${color},${date},${seller_id})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  
  const deleteFlower = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM Flowers WHERE flower_id = ${id} AND seller_id = ${seller_id}`);
      if(result.stmt.changes === 0){
        throw new Error(`Flower "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the Flower "${id}": `+e.message)
    }
  }
  
  const updateFlower = async (id, props) => {

    const { type, alive, color } = props;
    try {
      let statement = "";
        statement.append = SQL`UPDATE Flowers SET  type=${type} , alive=${alive} , color = ${color} WHERE WHERE flower_id = ${id} AND seller_id = ${seller_id}`;
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the Flower ${id}: ` + e.message);
    }
  }



  const getFlower = async (id) => {
    try{
      const FlowerList = await db.all(SQL`SELECT Flower_id AS id, type,alive,color FROM Flowers WHERE Flower_id = ${id}`);
      const Flower = FlowerList[0]
      return Flower
    }catch(e){
      throw new Error(`couldn't get the Flower ${id}: `+e.message)
    }
  }
  
  const getAllFlowers = async props => {
    const { orderBy, seller_id, desc } = props
    try {
      const statement = SQL`SELECT flower_id AS id, type, alive,color, seller_id FROM flowers`;
      if(seller_id){
        statement.append(SQL` WHERE seller_id = ${seller_id}`)
      }
      const isValidOrderBy = /type|alive|date/.test(orderBy)
      if(isValidOrderBy){
        statement.append( desc? SQL` ORDER BY ${orderBy} DESC` : SQL` ORDER BY ${orderBy} ASC`);
      }
      const rows = await db.all(statement);

      return rows;
    } catch (e) {
      throw new Error(`couldn't retrieve flowers: ` + e.message);
    }
  };

  
  
  const controller = {
    addFlower,
    deleteFlower,
    updateFlower,
    getFlower,
    getAllFlowers,
    createUserIfNotExists,
    createUser

  }

  return controller
}


export default initializeDatabase



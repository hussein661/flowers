// back/src/index.js
import app from './app'
import initializeDatabase from './db'
import { isLoggedIn } from './auth'


const start = async () => {
  const controller = await initializeDatabase()
  
  app.get('/', (req, res, next) => res.send("ok"));

  // CREATE
  app.get('/flowers/add', async (req, res, next) => {
    const seller_id = req.user.sub
    const { type, alive,color} = req.query
    const result = await controller.addFlower({type,alive,color,seller_id})
    res.json({success:true, result})
  })

  // READ
  app.get('/flowers/get/:id', async (req, res, next) => {
    const { id } = req.params
    const Flower = await controller.getFlower(id)
    res.json({success:true, result:Flower})
  })

  // DELETE
  app.get('/flowers/delete/:id', async (req, res, next) => {
    const author_id = req.user.sub

    const { id } = req.params
    const result = await controller.deleteFlower(id,author_id)
    res.json({success:true, result})
  })

  // UPDATE
  app.get('/flowers/update/:id', async (req, res, next) => {
    const author_id = req.user.sub

    const { id } = req.params
    const { type, alive,color } = req.query
    const result = await controller.updateFlower(id,{type,alive,color,author_id})
    res.json({success:true, result})
  })

  // LIST
  app.get('/flowers/list', async (req, res, next) => {
    const { order, desc } = req.query;
    const flowers = await controller.getAllFlowers(order,desc)
    res.json(flowers)
  })

  app.get('/mypage', isLoggedIn, async ( req, res, next ) => {
    try{
      const { order, desc } = req.query;
      const { sub, nickname} = req.user
      const user = await controller.createUserIfNotExists({sub, nickname})
      const flowers = await controller.getflowersList({order, desc, seller_id})
      user.flowers = flowers
      res.json({ success: true, result: user });
    }catch(e){
      next(e)
    }
  })


  app.listen(8080, () => console.log('server listening on port 8080'))
}

start()


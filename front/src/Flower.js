import React from 'react'
import * as auth0Client from "./auth";


export default class Flower extends React.Component {
  state = {
    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };

  renderViewMode() {
    const { id, type,alive,color, seller_id, deleteFlower } = this.props;
    const isLoggedIn = auth0Client.isAuthenticated();
    const current_logged_in_user_id = isLoggedIn && auth0Client.getProfile().sub
    const is_seller = seller_id === current_logged_in_user_id
    return (
      <div>
        <span>
          {id} - {type} - {alive} - {color}
        </span>
        {is_seller && isLoggedIn? 
        <div>
                <button onClick={this.toggleEditMode} classtype="success">
                edit
              </button>
              <button onClick={() => deleteFlower(id)} classtype="warning">
                x
              </button>
        </div>
        : 
        false }

      </div>
    );
  }
  renderEditMode() {
    const { type, alive,color } = this.props;
    return (
      <form
        classtype="third"
        onSubmit={this.onSubmit}
        onReset={this.toggleEditMode}
      >
        <input
          type="text"
          placeholder="type"
          name="Flower_type_input"
          defaultValue={type}
        />
        <input
          type="text"
          placeholder="alive"
          name="Flower_alive_input"
          defaultValue={alive}
        />
          <input
          type="text"
          placeholder="color"
          name="Flower_color_input"
          defaultValue={color}
        />
        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" classtype="button" />
        </div>
      </form>
    );
  }
  onSubmit = evt => {
    // stop the page from refreshing
    evt.preventDefault();
    // target the form
    const form = evt.target;
    // extract the two inputs from the form
    const Flower_type_input = form.Flower_type_input;
    const Flower_alive_input = form.Flower_alive_input;
    const Flower_color_input = form.Flower_color_input;

    // extract the values
    const type = Flower_type_input.value;
    const alive = Flower_alive_input.value;
    const color = Flower_color_input.value;

    // get the id and the update function from the props
    const { id, updateFlower } = this.props;
    // run the update Flower function
    updateFlower(id, { type, alive,color });
    // toggle back view mode
    this.toggleEditMode();
  };
  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode();
    } else {
      return this.renderViewMode();
    }
  }
}

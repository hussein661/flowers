import React, { Component } from "react";
import { withRouter, Route, Switch, Link } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Flower from "./Flower";
import  {Transition } from 'react-spring'
import { pause ,makeRequestUrl } from "./utils.js";
import "./App.css";
import * as auth0Client from "./auth";
import FlowerList from './FlowerList'
import SecuredRoute from './SecuredRoute'
import IsAuthenticated from './IsAuthenticated'



const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

class App extends Component {
  state = {
    flowers_list: [],
    error_message: "",
    type: "",
    alive: null,
    color: "",
    isLoading: false,
    checkingSession:true

  };

    isLogging = false;
    login = async () => {
      if (this.isLogging === true) {
        return;
      }
      this.isLogging = true;
      try {
        await this.getPersonalPageData(); // get the data from our server
        this.props.history.push("/profile");
      } catch (err) {
        this.isLogging = false
        toast.error(`error from the server: ${err.message}`);
      }
    };
    handleAuthentication = () => {
      this.login();
      return <p>wait...</p>;
    };

    getPersonalPageData = async () => {
      try {
        const url = makeUrl(`mypage`);
        const response = await fetch(url, {
          headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
        });
        const answer = await response.json();
        if (answer.success) {
          const user = answer.result;
          this.setState({user})
          if(user.firstTime){
            toast(`welcome ${user.nickname}! We hope you'll like it here'`);
          }
          toast(`hello ${user.nickname}'`);
        }
      
      } catch (err) {
        this.setState({ error_message: err.message });
        toast.error(err.message);
      }
    };
  getflower = async id => {
    // check if we already have the flower
    const previous_flower = this.state.flowers_list.find(
      flower => flower.id === id
    );
    if (previous_flower) {
      return; // do nothing, no need to reload a flower we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/flowers/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of flowers
        const flower = answer.result;
        const flowers_list = [...this.state.flowers_list, flower];
        this.setState({ flowers_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  deleteFlower = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/flowers/delete/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const flowers_list = this.state.flowers_list.filter(
          flower => flower.id !== id
        );
        this.setState({ flowers_list });
        toast("flower deleted")
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  updateFlower = async (id, props) => {
    try {

      const response = await fetch(
        `http://localhost:8080/flowers/update/${id}?type=${props.type}&alive=${props.alive}&color=${props.color}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const flowers_list = this.state.flowers_list.map(flower => {
          // if this is the flower we need to change, update it. This will apply to exactly
          // one flower2
          if (flower.id === id) {
            const new_flower = {
              id: flower.id,
              type: props.type || flower.type,
              alive: props.alive || flower.alive,
              color: props.color || flower.color
            };
            toast("flower updated")
            return new_flower;

          }
          // otherwise, don't change the flower at all
          else {
            return flower;

          }
        });
        this.setState({ flowers_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  createFlower = async props => {
    try {

      const { type, alive, color } = props;
      const response = await fetch(
        `http://localhost:8080/flowers/add/?type=${type}&alive=${alive}&color=${color}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const flower = { type, alive,color, id };
        const flowers_list = [...this.state.flowers_list, flower];
        this.setState({ flowers_list });
        toast("flower added")
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  getFlowersList = async order => {
    try {
      this.setState({ isLoading: true });
      const response = await fetch(
        `http://localhost:8080/flowers/list?order=${order}`
      );
      await pause()
      const answer = await response.json();
      if (answer) {
        const flowers_list = answer;
        this.setState({ flowers_list, isLoading: false });
        toast("flower loaded")

      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

   componentDidMount() {
    this.getFlowersList();
    if (this.props.location.pathname === '/callback'){
      return
    }
    try {
      auth0Client.silentAuth();
    } catch (err) {
      if (err.error !== 'login_required'){
        console.log(err.error);
      }
    }
    this.setState({checkingSession:false});

  }



  onSubmit = evt => {
    // stop the form from submitting:
    evt.preventDefault();
    // extract type and alive from state
    const { type, alive, color } = this.state;
    // create the flower from mail and alive
    this.createFlower({ type, alive, color });
    // empty type and alive so the text input fields are reset
    this.setState({ type: "", alive: "", color: "" });
  };
  renderHomePage = () => {
    const { flowers_list, error_message, isLoading } = this.state;
    return (
      <div classtype="App">
        {error_message ? <p> ERROR! {error_message}</p> : false}

        {isLoading ? (
          <p>loading...</p>
        ) : (
            <Transition
              items={flowers_list}
              keys={flower => flower.id}
              from={{ transform: "translate3d(-100px,0,0)" }}
              enter={{ transform: "translate3d(0,0px,0)" }}
              leave={{ transform: "translate3d(-100px,0,0)" }}
            >
              {flower => style => (
                <div style={style}>
                  <Flower
                    key={flower.id}
                    id={flower.id}
                    type={flower.type}
                    alive={flower.alive}
                    color={flower.color}
                    updateFlower={this.updateFlower}
                    deleteFlower={this.deleteFlower}
                  />
                </div>
              )}
            </Transition>
          )}


        <ToastContainer />

      </div>
    );
  }
  renderProfilePage = () => {
    if(this.state.checkingSession){
      return <p>validating session...</p>
    }
    return (
      <div>
        <p>profile page</p>
        {this.renderUser()}
      </div>
    );
  };


  renderCreateForm = () => {

    return(
      <form classtype="third" onSubmit={this.onSubmit}>
      <input
        type="text"
        placeholder="type"
        onChange={evt => this.setState({ type: evt.target.value })}
        value={this.state.type}
        />
      <input
        type="text"
        placeholder="alive"
        onChange={evt => this.setState({ alive: evt.target.value })}
        value={this.state.alive}
        />
      <input
        type="text"
        placeholder="color"
        onChange={evt => this.setState({ color: evt.target.value })}
        value={this.state.color}
        />
      <div>
        <input type="submit" value="ok" />
        <input type="reset" value="cancel" classtype="button" />
      </div>
    </form>
        )
    }



   renderContent() {
      if (this.state.isLoading) {
        return <p>loading...</p>;
      }
      return (
      <Switch>
        <Route path="/" exact render={this.renderHomePage} />
        <Route path="/Flower/:id" render={this.renderFlowerPage} />
        <Route path="/profile" render={this.renderProfilePage} />
        <SecuredRoute path="/create" render={this.renderCreateForm} checkingSession={this.state.checkingSession}/>
        <Route path="/callback" render={this.handleAuthentication} />

        <Route render={() => <div>not found!</div>} />
      </Switch>
    )
  }

  renderUser() {
    const isLoggedIn = auth0Client.isAuthenticated()
    if (isLoggedIn) {
      // user is logged in
      return this.renderUserLoggedIn();
    } else {
      return this.renderUserLoggedOut();
    }
  }
  renderUserLoggedOut() {
    return (
      <button onClick={auth0Client.signIn}>Sign In</button>
    );
  }
  renderUserLoggedIn() {
    const user_flowers = this.state.user.flowers.map(flowerFromUser =>
      this.state.flowers_list.find(
        flowerFromMain => flowerFromMain.id === flowerFromUser.id
      )
    );
    const nick = auth0Client.getProfile().name
    return <div>
    <FlowerList flowers_list={user_flowers} />

Hello, {nick}! <button onClick={()=>{auth0Client.signOut();this.setState({})}}>logout</button>
    </div>
  }


  render() {
    return (
      <div classtype="App">
        <div>
          <Link to="/">Home</Link> |<Link to="/profile">profile</Link> |
          <IsAuthenticated>
            <Link to="/create">create</Link>
          </IsAuthenticated>
        </div>
        {this.renderContent()}
        <ToastContainer />
      </div>)

  }
}

export default withRouter(App)


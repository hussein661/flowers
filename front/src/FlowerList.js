import React from "react";
import { Transition } from "react-spring"; // you can remove this from App.js
import { Link } from "react-router-dom";

const FlowerList = ({ flowers_list }) => (
  <Transition
    items={flowers_list}
    keys={flower => flower.id}
    from={{ transform: "translate3d(-100px,0,0)" }}
    enter={{ transform: "translate3d(0,0px,0)" }}
    leave={{ transform: "translate3d(-100px,0,0)" }}
  >
    { flower => style => (
      <div style={style}>
        <Link to={"/flower/"+flower.id}>{flower.name}</Link>
      </div>
    )}
  </Transition>
);

export default FlowerList